from django import forms

from .models import Newsletter, Subscriber


class SubscribeForm(forms.ModelForm):
    class Meta:
        model = Subscriber
        fields = ('email',)


class NewsletterForm(forms.ModelForm):
    class Meta:
        model = Newsletter
        exclude = ('expedition_datetime',)

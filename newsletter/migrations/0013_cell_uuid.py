from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('newsletter', '0012_cell_uuid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newslettersubscribeformcell',
            name='uuid',
            field=models.UUIDField(editable=False),
        ),
    ]

import uuid

from django.db import migrations


def forward(apps, schema_editor):
    for model in ['NewsletterSubscribeFormCell']:
        klass = apps.get_model('newsletter', model)
        for instance in klass.objects.filter(uuid__isnull=True):
            instance.uuid = uuid.uuid4()
            instance.save()


class Migration(migrations.Migration):
    dependencies = [
        ('newsletter', '0011_cell_uuid'),
    ]

    operations = [
        migrations.RunPython(forward, reverse_code=migrations.RunPython.noop),
    ]

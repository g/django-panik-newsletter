import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = []

    operations = [
        migrations.CreateModel(
            name='Newsletter',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('subject', models.CharField(max_length=50, verbose_name='Title')),
                ('date', models.DateField(verbose_name='Date')),
                ('text', ckeditor.fields.RichTextField(null=True, verbose_name='Text', blank=True)),
                (
                    'expedition_datetime',
                    models.DateTimeField(null=True, verbose_name='Expedition Date/time', blank=True),
                ),
            ],
            options={
                'ordering': ['date'],
                'verbose_name': 'Newsletter',
                'verbose_name_plural': 'Newsletters',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Subscriber',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('email', models.EmailField(unique=True, max_length=75)),
                ('inscription_date', models.DateField(auto_now_add=True)),
                ('is_validated', models.NullBooleanField()),
                ('is_registered', models.NullBooleanField()),
                ('password', models.CharField(max_length=100)),
            ],
            options={},
            bases=(models.Model,),
        ),
    ]

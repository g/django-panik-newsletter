from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('newsletter', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscriber',
            name='email',
            field=models.EmailField(unique=True, max_length=254),
        ),
    ]

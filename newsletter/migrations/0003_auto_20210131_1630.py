# Generated by Django 1.11.29 on 2021-01-31 16:30

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('newsletter', '0002_auto_20171002_2231'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscriber',
            name='email',
            field=models.EmailField(max_length=254),
        ),
    ]

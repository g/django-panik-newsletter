import gzip
import os
import re
import time
import xml.etree.ElementTree as ET
from datetime import datetime
from optparse import make_option

import urllib2
from django.conf import settings
from django.core.files import File
from django.core.management.base import BaseCommand, CommandError
from django.urls import reverse
from django.utils.html import strip_tags
from django.utils.text import slugify
from PIL import Image

from ...models import Subscriber


class Author:
    id_auteur = None
    email = None


class Command(BaseCommand):
    args = 'filename'
    help = 'Load newsletter subscribers from a Spip dump file'

    def handle(self, filename, verbosity, **options):
        self.verbose = verbosity > 1

        with open(filename) as fd:
            if self.verbose:
                print('Reading SPIP dump file')
            content = fd.read()
            # the spip_courriers parts of the spip export are not properly
            # encoded, we manually remove them here so the XML file can be
            # parsed correctly.
            content = (
                content[: content.find('<spip_courriers>')]
                + content[content.rfind('</spip_courriers>') + 17 :]
            )
            self.root = ET.fromstring(content)

            self.load_authors()

            if self.verbose:
                print('Creating subscribers')
            for author_xml in self.root.iter('spip_auteurs_elargis'):
                if author_xml.find('spip_listes_format').text not in ('texte', 'html'):
                    continue
                author = self.authors.get(author_xml.find('id_auteur').text)
                if author.email is None:
                    continue

                try:
                    Subscriber.objects.get(email=author.email)
                except Subscriber.DoesNotExist:
                    pass
                else:
                    continue

                subscriber = Subscriber()
                subscriber.email = author.email
                subscriber.is_validated = True
                subscriber.is_registered = False
                subscriber.password = 'xxx'
                subscriber.save()

    def load_authors(self):
        self.authors = {}
        for author_xml in self.root.iter('spip_auteurs'):
            author = Author()
            for attr in ('id_auteur', 'email'):
                setattr(author, attr, author_xml.find(attr).text)
            self.authors[author.id_auteur] = author

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from ...models import Subscriber


class Command(BaseCommand):
    def handle(self, verbosity, **kwargs):
        self.verbose = verbosity > 1

        qs = Subscriber.objects.exclude(is_registered=True).exclude(is_rejected=True)
        if getattr(settings, 'NEWSLETTER_SEND_CONFIRMATION', True):
            qs = qs.filter(is_validated=True)

        for subscriber in qs:
            if self.verbose:
                print('subscribing', subscriber.email)
            subscriber.subscribe()

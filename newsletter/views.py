import datetime
import email
import hashlib
import random
import socket

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.db import IntegrityError, models
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils.http import urlencode
from django.utils.translation import gettext as _
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import RedirectView, TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView

from .forms import NewsletterForm, SubscribeForm
from .models import Newsletter, Subscriber


class SubscribeView(CreateView):
    form_class = SubscribeForm
    model = Subscriber
    success_url = reverse_lazy('newsletter-thanks')

    def form_valid(self, form):
        resp = super().form_valid(form)
        # anti-bot checkboxes, if triggered, do not send confirmation email
        # but redirect to success page nevertheless
        self.object.bot_check1 = bool('validation' in self.request.POST)
        self.object.bot_check2 = bool('validation2' not in self.request.POST)
        # keep various data, for bot stats
        self.object.source_ip = (
            self.request.headers.get('x-forwarded-for')
            or self.request.headers.get('x-real-ip')
            or self.request.META.get('REMOTE_ADDR')
        )
        self.object.user_agent = self.request.headers.get('user-agent')
        self.object.email = self.object.email.lower()
        self.object.save()
        if not self.object.is_from_bot() and getattr(settings, 'NEWSLETTER_SEND_CONFIRMATION', True):
            self.object.send_confirmation_email()
        if self.request.headers.get('x-requested-with') == 'XMLHttpRequest':
            return JsonResponse({'err': 0})
        return resp


subscribe = SubscribeView.as_view()


class ThanksView(TemplateView):
    template_name = 'newsletter/thanks.html'


thanks = ThanksView.as_view()


class ConfirmationView(TemplateView):
    template_name = 'newsletter/done.html'

    def get_context_data(self, token, **kwargs):
        context = super().get_context_data(**kwargs)
        subscriber = get_object_or_404(Subscriber, password=token, is_validated=False)
        subscriber.is_validated = True
        subscriber.save()
        return context


confirmation = ConfirmationView.as_view()


class NewsletterCreateView(CreateView):
    form_class = NewsletterForm
    model = Newsletter

    success_url = reverse_lazy('newsletter-list')

    def get_form(self, *args, **kwargs):
        if not self.request.user.has_perm('newsletter.add_newsletter'):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)

    def get_initial(self):
        initial = super().get_initial()
        initial['date'] = datetime.datetime.today()
        return initial


newsletter_create = NewsletterCreateView.as_view()


class NewsletterListView(ListView):
    model = Newsletter

    def get_queryset(self):
        return super().get_queryset().order_by('-date')


newsletter_list = NewsletterListView.as_view()


class NewsletterDetailView(DetailView):
    model = Newsletter


newsletter_view = NewsletterDetailView.as_view()


class NewsletterUpdateView(UpdateView):
    form_class = NewsletterForm
    model = Newsletter

    def get_form(self, *args, **kwargs):
        if not self.request.user.has_perm('newsletter.add_newsletter'):
            raise PermissionDenied()
        return super().get_form(*args, **kwargs)


newsletter_update = NewsletterUpdateView.as_view()


class NewsletterSendView(RedirectView):
    permanent = False

    def get_redirect_url(self, pk):
        if not self.request.user.has_perm('newsletter.add_newsletter'):
            raise PermissionDenied()

        newsletter = get_object_or_404(Newsletter, pk=pk)
        newsletter.send()
        return reverse_lazy('newsletter-list')


newsletter_send = NewsletterSendView.as_view()


@csrf_exempt
def stats(request):
    return JsonResponse({'data': {'registered': Subscriber.objects.filter(is_registered=True).count()}})

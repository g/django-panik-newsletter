from django.conf import settings
from django.urls import path, re_path

from .views import (
    confirmation,
    newsletter_create,
    newsletter_list,
    newsletter_send,
    newsletter_update,
    newsletter_view,
    stats,
    subscribe,
    thanks,
)

urlpatterns = [
    path('', subscribe, name='newsletter-subscribe'),
    path('thanks/', thanks, name='newsletter-thanks'),
    re_path(r'^(?P<token>[0-9a-f]{40})$', confirmation, name='newsletter-confirmation'),
    path('stats/', stats),
]

management_patterns = [
    path('', newsletter_list, name='newsletter-list'),
    path('add', newsletter_create, name='newsletter-create'),
    path('<int:pk>/', newsletter_view, name='newsletter-view'),
    path('<int:pk>/edit', newsletter_update, name='newsletter-edit'),
    path('<int:pk>/send', newsletter_send, name='newsletter-send'),
]
